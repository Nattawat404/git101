def selection_sort(numbers):
    sorted_numbers = []
    while numbers:
        smallest_index = find_smallest_index(numbers)
        smallest_number = numbers.pop(smallest_index)
        sorted_numbers.append(smallest_number)

    return sorted_numbers

def find_smallest_index(numbers):
    smallest_index = 0
    for i in range(len(numbers)):
        if numbers[i] < numbers[smallest_index]:
            smallest_index = i

    return smallest_index