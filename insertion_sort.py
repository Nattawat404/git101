def insertion_sort(numbers):
    sorted_numbers = []
    for number in numbers:
        insert_at_index(sorted_numbers, number)

    return sorted_numbers

def insert_at_index(numbers, number):
    for i in range(len(numbers)):
        if number < numbers[i]:
            numbers.insert(i, number)
            break